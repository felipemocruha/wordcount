(ns wordcount
  (:import (java.util Arrays)
           (org.apache.beam.sdk Pipeline)
           (org.apache.beam.sdk.io TextIO)
           (org.apache.beam.sdk.options PipelineOptions
                                        PipelineOptionsFactory)
           (org.apache.beam.sdk.transforms Count
                                           Filter
                                           FlatMapElements
                                           MapElements)
           (org.apache.beam.sdk.values TypeDescriptors))
  (:gen-class))

;(set! *warn-on-reflection* true)

(defn line->arrays [^String line]
    (-> (.split line "[^\\p{L}]+")
        (.asList Arrays)))
    
(defn make-printable-string [word-count]
  (str (.getKey word-count) ": " (.getValue word-count)))

(def strs-desc (.. TypeDescriptors strings))

(defn -main [& args]
  (let [pipeline (Pipeline/create (PipelineOptionsFactory/create))]

    (-> pipeline
        (.apply (.from (TextIO/read) "lorem"))
        (.apply (.via (FlatMapElements/into strs-desc)) line->arrays)
        (.apply (Filter/by empty?))
        (.apply (Count/perElement))
        (.apply (.via (MapElements/into strs-desc) make-printable-string))
        (.apply (.to (TextIO/write) "out/wordcounts")))

    (.waitUntilFinish (.run pipeline))))
